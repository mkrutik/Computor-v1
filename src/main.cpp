#include <iostream>

#include "PolynomialSolver.hpp"

void    usage()
{
    std::cout << "Usage: ./computor [polynomial equation]" << std::endl
        << "Example: ./computor \"5 * X^0 + 4 * X^1 - 9.3 * X^2 = 1 * X^0\"" << std::endl;
}

int main(int argc, char **argv)
{
    if (argc != 2)
    {
        usage();
        return (-1);
    }
    try {
        PolynomialSolver p(argv[1]);
        std::cout << "Reduced form: " << p.reduced() << std::endl;
        std::cout << "Polynomial degree: " << p.degree() << std::endl;

        auto res = p.solve();
        if (res.size() == 1)
            std::cout << "The solution is:" << std::endl << res[0] << std::endl;
        else
        {
            std::cout << "The solutions are:" << std::endl;
            for (const auto &v : res)
                std::cout << v << std::endl;
        }
    } 
    catch(std::exception &e)
    {
        std::cout << e.what() << std::endl;
    }
}