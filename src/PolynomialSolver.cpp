#include "PolynomialSolver.hpp"

#include <sstream>

PolynomialSolver::PolynomialSolver(const std::string &equation) : _degree('$')
{
    for (auto b = equation.begin(); b != equation.end(); b++)
        if (isalpha(*b))
        {
            _letter = *b;
            break;
        }
    
	initialize(equation);

    if (_data.size() == 0)
        throw PolyExcep("Nothing to solve - empty polynomial equation");
    
    _degree = _data.rbegin()->second.pow;
}

const std::vector<std::string>  PolynomialSolver::solve() const
{
	if (_data.size() == 1 && _degree == 0)
		throw PolyExcep("The equation has no indeterminates variable - nothing to solve");
    
    if (_degree == 1)
        return liner();
    else if (_degree == 2)
        return quadratic();
    
    throw PolyExcep("The polynomial degree is stricly greater than 2, I can't solve.");
}

const std::string  PolynomialSolver::reduced() const
{
	std::stringstream ss;
    bool first = true;

	for (const auto &v : _data)
	{
        double      coef = v.second.coefficient;
        unsigned int pow = v.second.pow;

        if (coef < 0)
        {
            ss << "- ";
            coef *= -1;
        }
        else if (!first)
            ss << "+ ";

        ss << coef;

        if (pow == 1)
		    ss << " * "<< _letter; 
        else if (pow >= 2)
            ss << " * " << _letter << "^" << pow;

        ss << " ";

        if (first)
            first = false;
	}
	ss << "= 0";

	return  ss.str();
}

int PolynomialSolver::degree() const
{
	return  _degree;
}

void	PolynomialSolver::initialize(const std::string &s)
{
	bool        eq_sign = false;
    bool        before_eq_sign = false;
    bool        after_eq_sign = false;
    size_t      index = 0;
	Variable    num;

    auto parse_x = [&](unsigned int &i){
        if (s[i] == '^')
        {
            if (isdigit(s[i + 1]))
            {
                num.pow = std::stoul(&s[++i], &index);
                i += index;
            }
            else
                throw PolyExcep(myerror(s, i));
        }
        else
            num.pow = 1;

        while (isspace(s[i]))
            i++;

        if (s[i] != '\0' && s[i] != '+' && s[i] != '-' && s[i] != '=')
            throw PolyExcep(myerror(s, i));
    };

    for (unsigned int i = 0; i < s.length(); )
    {
        num = {1, 0};

        if (isspace(s[i]))
            ++i;
        else if (s[i] == '=')
        {
            if (!before_eq_sign)
                throw PolyExcep(myerror(s, i));

            if (eq_sign)
                throw PolyExcep(myerror(s, i));
            eq_sign = true;
            i++;
        }
        else if (s[i] == _letter ||
            ((s[i] == '-' || s[i] == '+') && (s[i + 1] == _letter || (s[i + 1] == ' ' && s[i + 2] == _letter))))
        {
            if (s[i] == '-')
                num.coefficient *= -1;
            
            if (eq_sign)
            {
                after_eq_sign = true;
                num.coefficient *= -1;
            }
            else
                before_eq_sign = true;

            while (s[i] != _letter)
                ++i;

            parse_x(++i);
            add_variable(num);
        }
        else if (isdigit(s[i]) ||
            ((s[i] == '-' || s[i] == '+') && (isdigit(s[i + 1]) || (s[i + 1] == ' ' && isdigit(s[i + 2])))))
        {
            if (s[i] == '-')
                num.coefficient *= -1;
            
            if (eq_sign)
            {
                after_eq_sign = true;
                num.coefficient *= -1;
            }
            else
                before_eq_sign = true;
            
            while (!isdigit(s[i]))
                i++;

            num.coefficient *= std::stod(&s[i], &index);
            i += index;

            while (isspace(s[i]))
                i++;
            
            if (s[i] == '*')
            {
                ++i;
                while (isspace(s[i]))
                    i++;
                
                if (s[i] != '\0' && s[i] == _letter)
                    parse_x(++i);
                else
                    throw PolyExcep(myerror(s, --i));
            }
             
            add_variable(num);
            
            if (s[i] != '\0' && s[i] != '+' && s[i] != '-' && s[i] != '=')
                throw PolyExcep(myerror(s, i));
        }
        else
            throw PolyExcep(myerror(s, i));
    }

    if (!eq_sign || !after_eq_sign)
        throw PolyExcep(myerror(s, s.length() - 1));
}

std::string	PolynomialSolver::myerror(std::string src, int i) const
{
    src.insert(i + 1, "\e[49m");
    src.insert(i, "\e[41m");
    src.insert(0, "\e[91mError Invalid polynomial:\e[39m   ");
    return src;
}

void	PolynomialSolver::add_variable(Variable v)
{
    if (v.coefficient == 0)
        return ;
	
    if (_data.find(v.pow) != _data.end())
	{
        _data[v.pow].coefficient += v.coefficient;
        if (_data[v.pow].coefficient == 0)
            _data.erase(_data.find(v.pow));
    }
	else	
		_data[v.pow] = v;
}

const std::vector<std::string>	PolynomialSolver::liner() const
{
    std::vector<std::string> res;
    std::stringstream ss;

    if (_data.size() == 2)
        ss << ((_data.at(0).coefficient * -1) / (_data.at(1).coefficient));
    else
        ss << 0;

    res.push_back(ss.str());
    return res;
}

const std::vector<std::string>	PolynomialSolver::quadratic() const
{
    std::vector<std::string>    res;

    auto mysqrt = [](double num) {
        double tmp = 0;
        double res = num / 2;
    
        while (res != tmp)
        {
            tmp = res;
            res = (tmp + num / tmp) / 2;
        }
    
        return res;
    };

    double a = _data.at(2).coefficient;
    double b = (_data.find(1) != _data.end()) ? _data.at(1).coefficient : 0;
    double c = (_data.find(0) != _data.end()) ? _data.at(0).coefficient : 0;

    double D = (b * b) - (4 * a * c);

    if (D == 0)
        res.push_back(std::to_string((-b) / (2 * a)));
    else if (D < 0)
    {
        double f = (b / (2 * a));
        if (f != 0)
            f *= -1;
        double s = (mysqrt(-D) / (2 * a));
        
        std::stringstream ss;

        if (f != 0)
            ss << f << " + ";

        ss << s << " * i";
        res.push_back(ss.str());

        ss.str(std::string());
        
        if (f != 0)
            ss << f << " - ";
        ss << s << " * i";
        res.push_back(ss.str());
    }
    else 
    {
        res.push_back(std::to_string((-b - mysqrt(D)) / (2 * a)));
        res.push_back(std::to_string((-b + mysqrt(D)) / (2 * a)));
    }
    return res;
}

PolynomialSolver::PolyExcep::PolyExcep(const std::string &msg) : _msg(msg) {}

const char*	PolynomialSolver::PolyExcep::what() const throw()
{
	return _msg.c_str();
}
