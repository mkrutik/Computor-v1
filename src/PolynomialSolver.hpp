#pragma once

#ifndef POLYNOMIALSOLVER_HPP
#define POLYNOMIALSOLVER_HPP

#include <map>
#include <vector>
#include <exception>
#include <string>

class PolynomialSolver {
	public:
		PolynomialSolver(const std::string &equation);

		const std::vector<std::string>	solve() const;
		const std::string			reduced() const;
		int							degree() const;

		class PolyExcep : public std::exception {
			public:
				PolyExcep(const std::string &msg);

				virtual const char*	what() const throw();

			private:
				const std::string _msg;
		};

	private:
		using Variable = struct{
			double          coefficient;
			unsigned int    pow;
		};

		void			initialize(const std::string &equation);
		std::string		myerror(std::string src, int i) const;
		void			add_variable(Variable v);

		const std::vector<std::string>	liner() const;
		const std::vector<std::string>	quadratic() const;

		std::map<int, Variable>	_data;
		int						_degree;
		char					_letter;
};

#endif