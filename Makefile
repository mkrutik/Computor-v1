APP = computor

# colorized output
RED 	= \033[0;31m
GREEN 	= \033[1;32m
CYAN	= \033[0;36m
ORANGE 	= \033[0;33m
GRAY 	= \033[1;30m
DEF 	= \033[0m


SRCDIR = src
OBJDIR = obj
BINDIR = bin

SRC = 	main.cpp \
		PolynomialSolver.cpp

OBJ = $(SRC:.cpp=.o)
COBJ = $(addprefix $(OBJDIR)/, $(OBJ))

COMPILER = clang++

FLAGS = -g -std=c++11 -Wall -Wextra -Werror 

.PHONY: all clear fclean re

all: init $(COBJ)
	@ $(COMPILER) $(FLAGS) $(COBJ) -o $(BINDIR)/$(APP)
	@ echo "${GREEN}Compile binary file was successful${DEF}"

$(OBJDIR)/%.o : $(SRCDIR)/%.cpp
	@ $(COMPILER) $(FLAGS) -o $@ -c $<
	@ echo "${GRAY}Compile object files${DEF}"

clean:
	@ rm -rf $(OBJDIR)
	@ echo "${ORANGE}Object files was successfuly remowed${DEF}"

fclean: clean
	@ rm -rf $(BINDIR)
	@ echo "${RED}Binary file was successfuly remowed${DEF}"

re: fclean all

init:
	@ echo "${CYAN}Creating folders was successful${DEF}"
	@ mkdir -p bin
	@ mkdir -p obj